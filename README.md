#surf-api

Just a simple API ecosystem deployed into Heroku: heroku.com

Ready to go as a starter project for API based projects

Using json server: npm install -g json-server

run json server: json-server --watch db.json

In my case to push into Heroku I had to use: git push heroku HEAD:master

Deployed on Heroku: https://my-surf-api.herokuapp.com/ | https://git.heroku.com/my-surf-api.git
Code base in Ania Kudów video tutorial: https://www.youtube.com/watch?v=FLnxgSZ0DG4&t=604s&ab_channel=CodewithAniaKub%C3%B3w
